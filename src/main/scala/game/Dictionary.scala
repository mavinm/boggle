package game

import tree.TernaryTree

/**
  * Created by mavin.martin on 3/10/16.
  */
class Dictionary(words: Set[String]) {

  val ternaryTree = new TernaryTree
  for (word <- words) ternaryTree.add(word.toUpperCase)

  /**
    * Checks to see if the word being passed in can form a valid word.
    *
    * Ex:
    *
    * If dictionary contains only word 'apple'
    * 'a' -> true
    * 'ap' -> true
    * 'app' -> true
    * 'appl' -> true
    * 'apple' -> true
    * 'appleg' -> false
    * 'pple' -> false
    * 'p' -> false
    *
    * @param word word
    * @return true if path is moving towards valid word, else false
    */
  def isValidPath(word: String): Boolean = ternaryTree.possibleSolution(word)

  /**
    * See's if the word is inside of the dictionary
    *
    * @param word word
    * @return true if word exists, else false
    */
  def contains(word: String): Boolean = ternaryTree.contains(word)
}
