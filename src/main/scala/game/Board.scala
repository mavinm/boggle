package game

import scala.collection.mutable

/**
  * 4x4 Boggle Board
  * Created by mavin.martin on 3/10/16.
  */
class Board(letters: String) {

  val letterPieces = Array.ofDim[Piece](4, 4)
  handleLetters()

  /**
    * Checks to see if the word is valid on the board
    * @param word
    * @return
    */
  def validWordOnBoard(word: String): Boolean = {
    // Words must be at least 3 in length
    if (word.length < 3) return false
    validWord(word.toUpperCase)
  }

  /**
    * See's if the word is valid on the board
    *
    * @param word word
    * @return true if exists on board, else false
    */
  private def validWord(word: String): Boolean = {
    def validWord(p: Piece, visited: Set[Piece], wordIndex: Int): Boolean = {
      if (visited.contains(p)) return false
      else if (wordIndex >= word.length) return true
      else if (word(wordIndex) != p.value) return false

      for (n <- p.neighbors if validWord(n, visited + p, wordIndex + 1)) return true
      false
    }

    for (p <- letterPieces.flatten if validWord(p, Set.empty, 0)) return true
    false
  }

  /**
    * Finds all valid words on the board that appear in the dictionary
    * @return
    */
  def findAllValidWords: Set[String] = {
    val results = new mutable.HashSet[String]

    def allLegalWords(p: Piece, visited: Set[Piece], currentWord: String): Unit = {
      for (neighbor <- p.neighbors if !visited.contains(neighbor)) {
        val appended = currentWord + neighbor
        if (Game.dictionary.contains(appended)) results += appended
        if (Game.dictionary.isValidPath(currentWord + neighbor)) {
          allLegalWords(neighbor, visited + neighbor, appended)
        }
      }
    }

    for (p <- letterPieces.flatten) allLegalWords(p, Set(p), p)
    results.toSet
  }

  override def toString: String = {
    var result = ""
    for (row <- 0 until 4) {
      for (col <- 0 until 4) {
        result += s"${
          letterPieces(row)(col)
        } "
      }
      result += "\n"
    }
    result
  }

  /**
    * Initiating function to setup the letter pieces on the board and set the neighbors to each piece
    */
  private def handleLetters(): Unit = {
    def getNeighbors(row: Int, col: Int): Set[Piece] = {
      val results = new mutable.HashSet[Piece]
      for (i <- row - 1 to row + 1) {
        for (j <- col - 1 to col + 1) {
          // Checks out of bounds
          if (!(i < 0 || i > 3 || j < 0 || j > 3)) {
            if (!(i == row && j == col)) {
              results += letterPieces(i)(j)
            }
          }
        }
      }
      results.toSet
    }

    // Stores pieces in matrix
    for ((c, i) <- letters.toUpperCase.zipWithIndex) {
      val dx = i % 4
      val dy = i / 4
      val piece = new Piece(c)
      letterPieces(dy)(dx) = piece
    }

    // Handles adding neighbors to pieces
    for (y <- 0 until 4) {
      for (x <- 0 until 4) {
        letterPieces(y)(x).neighbors = getNeighbors(y, x)
      }
    }
  }
}
