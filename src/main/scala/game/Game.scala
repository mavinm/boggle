package game

import scala.io.Source

/**
  * Created by mavin.martin on 3/10/16.
  */
object Game {

  var dictionary: Dictionary = _
  var board: Board = _

  def main(args: Array[String]) {
    if (args.length == 0) throw new Error("Specify the letters of the board 16 in length. Ex: 'ATSOERIYFAUZFBRN'")
    val letters = args(0)
    if (letters.length != 16) {
      throw new Error(s"Length of board letters needs to be 16 in length. '$letters' was length ${letters.length}")
    }
    val words = Source.fromURL(getClass.getResource("/words.txt")).mkString

    // Place any word larger than 3 inside dictionary
    val wordsLargerThanThree = words.split("\n").filter(_.length >= 3)
    dictionary = new Dictionary(wordsLargerThanThree.toSet)

    board = new Board(letters)
    println("===== BOARD =====")
    println(board)

    println("===== NEIGHBORS =====")
    for (p <- board.letterPieces.flatten) println(s"$p : ${p.neighbors}")
    println()

    println("===== VALID WORDS =====")
    println(board.findAllValidWords)
  }
}
