package game

/**
  * Created by mavin.martin on 3/10/16.
  */
object Piece {
  implicit def PieceToString(p: Piece): String = p.toString
}

class Piece(val value: Char) {
  var neighbors: Set[Piece] = _

  override def toString: String = value.toString
}
