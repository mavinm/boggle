package tree

/**
  * Created by mavin.martin on 3/10/16.
  *
  * Tree with a left, middle, and right node.
  * Holds words in the tree for search lookups
  *
  * MODIFIED TO MEET THE NEEDS OF BOGGLE GAME
  */
class TernaryTree {

  private var root: Node = _

  def add(value: String): Unit = {
    if (value == null || value.isEmpty) throw new Exception("Enter a valid string")
    root = add(root, value, 0)
  }

  /**
    * See's if value queried is a possible word inside of the tree
    *
    * @param value 'tre' is a subset of 'tree'
    * @return
    */
  def possibleSolution(value: String): Boolean = {
    val returnNode: Node = contains(root, value, 0)
    if (returnNode == null) false
    else true
  }

  /**
    * See's if ternary tree holds the particular word
    *
    * @param value
    * @return
    */
  def contains(value: String): Boolean = {
    val returnNode: Node = contains(root, value, 0)
    if (returnNode == null) false
    else returnNode match {
      case Mid(_) => false
      case End(_) => true
    }
  }

  private def add(current: Node, value: String, index: Int): Node = {
    val char: Char = value.charAt(index)
    var returnNode: Node = current
    if (returnNode == null) returnNode = new Mid(char)

    if (char < returnNode.char) {
      returnNode.left = add(returnNode.left, value, index)
    } else if (char > returnNode.char) {
      returnNode.right = add(returnNode.right, value, index)
    } else if (index < value.length - 1) {
      returnNode.mid = add(returnNode.mid, value, index + 1)
    } else {
      val result = End(char)
      result.left = returnNode.left
      result.mid = returnNode.mid
      result.right = returnNode.right
      returnNode = result
    }

    returnNode
  }

  private def contains(current: Node, value: String, index: Int): Node = {
    if (current == null) return null
    val char: Char = value.charAt(index)
    if (char < current.char) {
      contains(current.left, value, index)
    } else if (char > current.char) {
      contains(current.right, value, index)
    } else if (index < value.length - 1) {
      contains(current.mid, value, index + 1)
    } else {
      current
    }
  }

  trait Node {
    val char: Char
    var left: Node = null
    var mid: Node = null
    var right: Node = null
  }

  /**
    * Any node containing a char
    *
    * @param char character in node
    */
  private case class Mid(char: Char) extends Node

  /**
    * End of word
    *
    * @param char character on leaf
    */
  private case class End(char: Char) extends Node

}

