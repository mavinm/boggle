package game

import game.Game._
import junit.JUnitFunSuite
import org.scalatest.BeforeAndAfter

import scala.io.Source

/**
  * Created by mavin.martin on 3/23/16.
  */
class BoardTest extends JUnitFunSuite with BeforeAndAfter {

  before {
    val words = Source.fromURL(getClass.getResource("/words.txt")).mkString

    // Place any word larger than 3 inside dictionary
    val wordsLargerThanThree = words.split("\n").filter(_.length >= 3)
    dictionary = new Dictionary(wordsLargerThanThree.toSet)
  }

  def buildBoard(letters:String):Board = {
    new Board(letters.trim.replaceAll("[\n ]", ""))
  }

  test("word exists on board") {
    val letters =
      """
        |T E S T
        |X X X M
        |A X T H
        |M X T A
      """.stripMargin

    val b = buildBoard(letters)
    assert(b.validWordOnBoard("TEST"))
    assert(b.validWordOnBoard("THAT"))
    assert(b.validWordOnBoard("HAT"))
    assert(b.validWordOnBoard("XXXX"))
    assert(b.validWordOnBoard("XXXXX"))
    assert(!b.validWordOnBoard("MAM"))
    assert(!b.validWordOnBoard("MM"))
    assert(!b.validWordOnBoard("AH"))
  }

  test("all words output") {
    val letters =
      """
        |A T S O
        |E R I Y
        |F A U Z
        |F B R N
      """.stripMargin

    val b = buildBoard(letters)
    val results = b.findAllValidWords

    for (word <- results) {
      assert(b.validWordOnBoard(word), word)
    }
  }
}
