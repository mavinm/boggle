package game

import junit.JUnitFunSuite
import org.scalatest.BeforeAndAfter

import scala.io.Source

/**
  * Created by mavin.martin on 3/10/16.
  */
class DictionaryTest extends JUnitFunSuite with BeforeAndAfter {
  var dictionary: Dictionary = _
  var wordsArray: Array[String] = _

  before {
    val words = Source.fromURL(getClass.getResource("/words.txt")).mkString
    wordsArray = words.split("\n")
    dictionary = new Dictionary(wordsArray.toSet)
  }

  test("Load in dictionary") {
    for (word <- wordsArray) assert(dictionary.isValidPath(word.toUpperCase), word.toUpperCase())
    for (word <- wordsArray) assert(dictionary.contains(word.toUpperCase), word.toUpperCase)
  }

  test("Empty Word") {
    intercept[StringIndexOutOfBoundsException] {
      assert(dictionary.isValidPath(""))
    }
  }
}
