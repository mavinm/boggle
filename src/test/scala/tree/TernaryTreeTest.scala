package tree

import junit.JUnitFunSuite

/**
  * Created by mavin.martin on 3/10/16.
  */
class TernaryTreeTest extends JUnitFunSuite {

  def parseSentence(sentence: String) = sentence.split(" ")

  test("Placing values into tree and retrieving the same words back") {
    val sentence = "she sells sea shells by the sea shore"
    val tree = new TernaryTree
    val words = sentence.split(" ")

    for (word <- words) tree.add(word)
    for (word <- words) assert(tree.contains(word))
  }

  test("Going beyond the correct word") {
    val sentence = "she sells sea shells by the sea shore"
    val tree = new TernaryTree
    val words = sentence.split(" ")

    for (word <- words) tree.add(word)
    for (word <- words) assert(!tree.contains(word + "a"))
  }

  test("Not going far enough for the correct word") {
    val sentence = "she sells sea shells by the sea shore"
    val tree = new TernaryTree
    val words = sentence.split(" ")
    val wordsAsSet = words.toSet

    for (word <- words) tree.add(word)
    for (word <- words) {
      for (i <- 0 until word.length) {
        val test = word.dropRight(i)
        if (wordsAsSet.contains(test)) {
          assert(tree.contains(test))
        } else {
          assert(!tree.contains(test))
        }
      }
    }
  }

  test("Place inappropriate string") {
    val tests = List(null, "")
    val tree = new TernaryTree
    for (test <- tests) {
      intercept[Exception] {
        tree.add(test)
      }
    }
  }

  test("Subset of word exists in tree") {
    val sentence = "she sells sea shells by the sea shore"
    val tree = new TernaryTree
    val words = sentence.split(" ")

    for (word <- words) tree.add(word)
    for (word <- words) {
      for (i <- 0 until word.length) {
        assert(tree.possibleSolution(word.dropRight(i)))
      }
    }
  }
}
