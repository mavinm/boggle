package junit

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

/**
  * Created by mavin.martin on 3/25/16.
  */
@RunWith(classOf[JUnitRunner])
class JUnitFunSuite extends FunSuite
